var express = require('express');
const morgan = require('morgan');
const path = require('path');
var  fs = require('fs'); //
var https = require('https');//

var app = express();

//Para poder capturar los datos del formulario (sin urlencoded nos devuelve "undefined")
app.use(express.urlencoded({extended:false}));
app.use(express.json());//además le decimos a express que vamos a usar json

//Invocamos a dotenv
const dotenv = require('dotenv');
dotenv.config({ path: './env/.env'});

//eteamos el directorio de assets
app.use('/resources',express.static('public'));
app.use('/resources', express.static(__dirname + '/public'));

//Establecemos el motor de plantillas
app.set('view engine','ejs');

//Invocamos a bcrypt
const bcrypt = require('bcryptjs');

//variables de session
const session = require('express-session');
app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));

//Invocamos a la conexion de la DB
const connection = require('./database/db');

//Establecemos las rutas
app.get('/index',(req, res)=>{
  res.render('index');
})

app.get('/paginaInicio',(req, res)=>{
  res.render('paginaInicio',{
	login: false,
	title: 'Formulario'
});
})

//Metodo para la autenticacion
app.post('/auth', async (req, res)=> {
	const user = req.body.user;
	const pass = req.body.pass;    
    let passwordHash = await bcrypt.hash(pass, 8);
	if (user && pass) {
		connection.query(' SELECT * FROM stm_mae_usuario_usae WHERE ID_USUARIO_PK = ?', [user], async (error, results, fields)=> {
			if( results.length == 0 || !(await bcrypt.compare(pass, results[0].TXT_PASSWORD)) ) {    
				res.render('index', {
                        alert: true,
                        alertTitle: "Error",
                        alertMessage: "Usuario y/o contraseña incorrecto",
                        alertIcon:'error',
                        showConfirmButton: true,
                        timer: false,
                        ruta: 'index'    
                    });				
			} else {         
				//creamos una var de session y le asignamos true si INICIO SESSION       
				req.session.loggedin = true;                
				req.session.name = results[0].TXT_NOMBRE;
				req.session.userid= results[0].ID_USUARIO_PK;
				res.render('index', {
					alert: true,
					alertTitle: "Datos correctos",
					alertMessage: "¡Inicio de sesión correcto!",
					alertIcon:'success',
					showConfirmButton: false,
					timer: 1500,
					ruta: ''
				});        			
			}			
			res.end();
		});
	} else {	
		res.render('index', {
			alert: true,
			alertTitle: "Advertencia",
			alertMessage: "¡Por favor ingresa un usuario y/o contraseña!",
			alertIcon:'warning',
			showConfirmButton: true,
			timer: false,
			ruta: 'index'
		}); 
	}
});

//Método para controlar que está auth en todas las páginas
app.get('/', (req, res)=> {
	if (req.session.loggedin) {
		res.render('paginaInicio',{
			login: true,
			title: 'Pagina Bienvenida',
			name: req.session.name
		});
	} else{
		res.render('index',{
			login:false,
			name:'Debe iniciar sesión',
		});				
	}
	res.end();
});

//función para limpiar la caché luego del logout
app.use(function(req, res, next) {
    if (!req.user)
        res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    next();
});

 //Logout
//Destruye la sesión.
app.get('/logout', function (req, res) {
	req.session.destroy(() => {
	  res.redirect('index') // siempre se ejecutará después de que se destruya la sesión
	})
});

//Server seguro https
https.createServer({
	cert: fs.readFileSync('server.crt'),
	key: fs.readFileSync('server.key')
}, app).listen(4000, (req, res)=>{
    console.log('SERVER RUNNING IN https://localhost:4000');
});

// settings
app.set('views', path.join(__dirname, 'views'));

// middlewares
app.use(morgan('dev'));

//Rutas
	//Index
	app.use(require('./routes'));
	//Asignacion
	app.use(require('./routes/asignacionRoutes'));
	//Usuarios
	app.use(require('./routes/usuariosRoutes'));
	//Historial Servicios
	app.use(require('./routes/histServiciosRoutes'));
	//Clientes
	app.use(require('./routes/clientesRoutes'));
	//Formulario atencion
	app.use(require('./routes/formularioSatisfaccionRoutes'));
	//Servicios por atender
	app.use(require('./routes/serviciosPorAtender'));
	


// static files
app.use(express.static(path.join(__dirname, 'public')));
