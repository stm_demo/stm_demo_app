function validar(){
    var id = document.getElementById("id").value;
    var nombre = document.getElementById("nombre").value;
    var app = document.getElementById("app").value;
    var apm = document.getElementById("apm").value;
    var email = document.getElementById("email").value;
    var pass = document.getElementById("pass").value;
    var pass2 = document.getElementById("pass2").value;
    var selectTipoUsuario = document.getElementById("selectTipoUsuario").value;
    if(id.length == 7){
        if(nombre.trim().length>0){
            if(app.trim().length>0){
                if(apm.trim().length>0){
                            if(email.trim().length>0){
                                if(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email)){
                                    if(pass.trim().length>0){
                                        if(pass.length==8){
                                            if(pass == pass2){

                                            }else{
                                                Swal.fire({  
                                                    title: '¡Error!',
                                                    text: 'Las contraseñas no coinciden', 
                                                    icon:'error', 
                                                    showConfirmButton: true, 
                                                    timer: false,});
                                                    return false;
                                            }
                                        }else{
                                            if(pass.length >=1){
                                                Swal.fire({  
                                                    title: '¡Error!',
                                                    text: 'La contraseña debe tener 8 caracteres', 
                                                    icon:'error', 
                                                    showConfirmButton: true, 
                                                    timer: false,});
                                                    return false;
                                            } 
                                        }

                                    }else{
                                        if(pass.trim()==""){
                                            Swal.fire({  
                                                title: '¡Error!',
                                                text: 'La contraseña tiene un espacio al inicio', 
                                                icon:'error', 
                                                showConfirmButton: true, 
                                                timer: false,});
                                                return false;
                                            }
                                    }
                                    
                                }else{
                                    Swal.fire({  
                                        title: '¡Error!',
                                        text: 'El email tiene un formato incorrecto', 
                                        icon:'error', 
                                        showConfirmButton: true, 
                                        timer: false,});
                                        return false;
                                }

                            }
                            else{
                                if(email.trim()==""){
                                    Swal.fire({  
                                        title: '¡Error!',
                                        text: 'El email tiene un espacio al inicio', 
                                        icon:'error', 
                                        showConfirmButton: true, 
                                        timer: false,});
                                        return false;
                                    }
                            }

                        }
                        else{
                        if(apm.trim()==""){
                        Swal.fire({  
                            title: '¡Error!',
                            text: 'El apellido materno tiene un espacio al inicio', 
                            icon:'error', 
                            showConfirmButton: true, 
                            timer: false,});
                            return false;
                        }
                }

            }
            else{
                if(app.trim()==""){
                    Swal.fire({  
                        title: '¡Error!',
                        text: 'El apellido paterno tiene un espacio al inicio', 
                        icon:'error', 
                        showConfirmButton: true, 
                        timer: false,});
                        return false;
                    }
                }
        }
        else{
            if(nombre.trim()==""){
                Swal.fire({  
                    title: '¡Error!',
                    text: 'El nombre tiene un espacio al inicio', 
                    icon:'error', 
                    showConfirmButton: true, 
                    timer: false,});
                    return false;

            }
        }
    }
    else{
        if(id.length >=1){
            Swal.fire({  
                title: '¡Error!',
                text: 'La ID debe contener 7 caracteres', 
                icon:'error', 
                showConfirmButton: true, 
                timer: false,});
                return false;
        } 
    }
    
}
