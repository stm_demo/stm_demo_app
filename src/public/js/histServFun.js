//Data Tables - Clientes
$(document).ready(function () {
    tablaHistServicios = $('#tablaHistServicios').DataTable({
        //Cambio lenguaje a DataTable
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "infoThousands": ",",
            "loadingRecords": "Cargando...",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "sProcessing": "Procesando...",
        }
    });

    var fila;
    $(document).on("click", ".btnEditar", function () {
        fila = $(this).closest("tr");
        curp = parseInt(fila.find('td:eq(0)').text());
        /* resto de filas de la tabla*/

        $(".modal-header").css("background-color", "#00aeff");
        $(".modal-header").css("color", "#fff");
        $(".modal-title").text("Editar Cliente");
        $("#modalAcciones").modal("show");
    });
});


//1) Definir Las Variables Correspondintes
var opt_1 = new Array("Seleccione el Mes...", "Enero", "Febrero", "Marzo", "Abril",
    "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre", "");
var opt_2 = new Array("Seleccione el Bimestre...", "Enero - Febrero", "Marzo - Abril", "Mayo - Junio",
    "Julio - Agosto", "Septiembre - Octubre", "Noviembre - Diciembre", "");
var opt_3 = new Array("Seleccione el Semestre...", "Enero - Junio", "Julio - Diciembre", "");

function cambiarTipo() {
    var select;
    //Se toma el valor de "select seleccionada"
    select = document.formHistServFilt.selectTipo[document.formHistServFilt.selectTipo.selectedIndex].value;
    //se chequea si "select" esta definida
    if (select != 0) {
        //selecionamos las opt Correctas
        mis_opts = eval("opt_" + select);
        //se calcula el numero de cosas
        num_opts = mis_opts.length;
        //marco el numero de opt en el select
        document.formHistServFilt.selectPeriodo.length = num_opts;
        //para cada opt del array, la pongo en el select
        for (i = 0; i < num_opts; i++) {
            if (i == 1) {
                document.formHistServFilt.selectPeriodo.options[i] = this.disable;
            }
            document.formHistServFilt.selectPeriodo.options[i].value = i;
            document.formHistServFilt.selectPeriodo.options[i].text = mis_opts[i];
        }
    } else {
        //si no habia ninguna opt seleccionada, elimino las cosas del select
        document.formHistServFilt.selectPeriodo.length = 1;
        //ponemos un guion en la unica opt que he dejado
        document.formHistServFilt.selectPeriodo.options[0].value = "0";
        document.formHistServFilt.selectPeriodo.options[0].text = "Seleccione...";
    }
    //hacer un reset de las opts
    document.formHistServFilt.selectPeriodo.options[0].selected = true;

}