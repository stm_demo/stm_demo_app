$(document).ready(function() {
    
    document.getElementById("curp").addEventListener("input", (e) => {
        let value = e.target.value;
        e.target.value = value.replace(/[^A-Z\d-]/g, "");
      });
});
function validar(){
    var curp = document.getElementById("curp").value;
    var apepat = document.getElementById("apepat").value;
    var apemat = document.getElementById("apemat").value;
    var nombre = document.getElementById("nombre").value;
    var calle = document.getElementById("calle").value;
    var colonia = document.getElementById("colonia").value;
    var email = document.getElementById("email").value;
    var celular = document.getElementById("celular").value;
    var selectSexo = document.getElementById("selectSexo").value;
    var selectEstudio = document.getElementById("selectEstudio").value;
    var selectTipoCliente = document.getElementById("selectTipoCliente").value;
    if(curp.length == 18){
        if(apepat.trim().length>0){
            if(apemat.trim().length>0){
                if(nombre.trim().length>0){
                    if(calle.trim().length>0){
                        if(colonia.trim().length>0){
                            if(email.trim().length>0){
                                if(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email)){
                                    if(celular.trim().length>0){
                                        if(celular.length==10){

                                        }else{
                                            if(celular.length >=1){
                                                Swal.fire({  
                                                    title: '¡Error!',
                                                    text: 'El celular debe tener 10 caracteres', 
                                                    icon:'error', 
                                                    showConfirmButton: true, 
                                                    timer: false,});
                                            } 
                                        }

                                    }else{
                                        if(celular.trim()==""){
                                            Swal.fire({  
                                                title: '¡Error!',
                                                text: 'El celular tiene un espacio al inicio', 
                                                icon:'error', 
                                                showConfirmButton: true, 
                                                timer: false,});
                                            }
                                    }
                                    
                                }else{
                                    Swal.fire({  
                                        title: '¡Error!',
                                        text: 'El email tiene un formato incorrecto', 
                                        icon:'error', 
                                        showConfirmButton: true, 
                                        timer: false,});
                                }

                            }else{
                                if(email.trim()==""){
                                    Swal.fire({  
                                        title: '¡Error!',
                                        text: 'El email tiene un espacio al inicio', 
                                        icon:'error', 
                                        showConfirmButton: true, 
                                        timer: false,});
                                    }
                            }

                        }else{
                            if(colonia.trim()==""){
                                Swal.fire({  
                                    title: '¡Error!',
                                    text: 'La colonia tiene un espacio al inicio', 
                                    icon:'error', 
                                    showConfirmButton: true, 
                                    timer: false,});
                                }
                        }

                    }else{
                        if(calle.trim()==""){
                            Swal.fire({  
                                title: '¡Error!',
                                text: 'La calle tiene un espacio al inicio', 
                                icon:'error', 
                                showConfirmButton: true, 
                                timer: false,});
                            }
                    }

                }else{
                    if(nombre.trim()==""){
                        Swal.fire({  
                            title: '¡Error!',
                            text: 'El nombre tiene un espacio al inicio', 
                            icon:'error', 
                            showConfirmButton: true, 
                            timer: false,});
                        }
                }

            }else{
                if(apemat.trim()==""){
                    Swal.fire({  
                        title: '¡Error!',
                        text: 'El apellido materno tiene un espacio al inicio', 
                        icon:'error', 
                        showConfirmButton: true, 
                        timer: false,});
                    }
                }
        }
        else{
            if(apepat.trim()==""){
                Swal.fire({  
                    title: '¡Error!',
                    text: 'El apellido paterno tiene un espacio al inicio', 
                    icon:'error', 
                    showConfirmButton: true, 
                    timer: false,});

            }
        }
    }
    else{
        if(curp.length >=1){
            Swal.fire({  
                title: '¡Error!',
                text: 'El campo curp debe tener 18 caracteres', 
                icon:'error', 
                showConfirmButton: true, 
                timer: false,});
        } 
    }
    
}