modulo.exports = class StmMaeCliente {
    constructor (
        id_curp_pk,
        txt_nombre,
        txt_app,
        txt_apm,
        txt_calle,
        num_num,
        txt_col,
        txt_email,
        txt_cel,
        txt_sexo,
        txt_grado_estdio,
        bit_tipo_cliente,
        id_cct_fk
        )     
    {
        this.id_curp_pk = id_curp_pk;
        this.txt_nombre = txt_nombre;
        this.txt_app = txt_app;
        this.txt_apm = txt_apm;
        this.txt_calle = txt_calle;
        this.num_num = num_num;
        this.txt_col = txt_col;
        this.txt_email = txt_email;
        this.txt_cel = txt_cel;
        this.txt_sexo = txt_sexo;
        this.txt_grado_estdio = txt_grado_estdio;
        this.bit_tipo_cliente = bit_tipo_cliente;
        this.id_cct_fk = id_cct_fk;
    }
}