modulo.exports = class StmMaeCliente {
    constructor (
        id_usuario_pk,
        txt_nombre,
        txt_app,
        txt_apm,
        txt_email,
        txt_password,
        id_tipo_usuario_fk
        )     
    {
        this.id_usuario_pk = id_usuario_pk;
        this.txt_nombre = txt_nombre;
        this.txt_app = txt_app;
        this.txt_apm = txt_apm;
        this.txt_email = txt_email;
        this.txt_password = txt_password;
        this.id_tipo_usuario_fk = id_tipo_usuario_fk;
    }
}