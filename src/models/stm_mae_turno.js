module.exports = class StmMaeTurno{
    constructor(
        id_turno_pk,
        dsc_asunto,
        txt_estado,
        fch_fecha,
        hra_turno,
        txt_lugar,
        time_tiempo_servicio,
        id_curp_fk,
        id_empleado_fk,
        id_servicio_fk
    ){
        this.id_turno_pk = id_turno_pk;
        this.dsc_asunto = dsc_asunto;
        this.txt_estado = txt_estado;
        this.fch_fecha = fch_fecha;
        this.hra_turno = hra_turno;
        this.txt_lugar = txt_lugar;
        this.time_tiempo_servicio = time_tiempo_servicio;
        this.id_curp_fk = id_curp_fk;
        this.id_empleado_fk = id_empleado_fk;
        this.id_servicio_fk = id_servicio_fk;
    }
}