const connection = require('../database/db');
const controller = {};
const bcrypt = require('bcryptjs');

controller.users=(req,res)=> {
    connection.query('SELECT * FROM stm_mae_usuario_usae',(error,results)=>{
        if(error){
          throw error;
        }else{
          if(req.session.loggedin == true){
            name = req.session.name;
            res.render('usuarios' , {results:results, title: 'Usuarios',name:name}); 
          }else{
            res.render('index');
          }         
        }
    });
};


controller.addUser=async(req, res)=>{
    const ID_USUARIO_PK = req.body.id;
    const TXT_NOMBRE = req.body.nombre;
    const TXT_APP = req.body.app;
    const TXT_APM = req.body.apm;
    const TXT_EMAIL = req.body.email;
    const TXT_PASSWORD = req.body.pass;
    const pass2 = req.body.pass2;
    let passwordHash = await bcrypt.hash(TXT_PASSWORD, 8);
    const ID_TIPO_USUARIO_FK = req.body.selectTipoUsuario;
      connection.query('SELECT * FROM stm_mae_usuario_usae WHERE ID_USUARIO_PK = ? OR TXT_EMAIL = ?',[ID_USUARIO_PK,TXT_EMAIL],(error,results)=>{
        if (error) {
          res.json(error);
        }else{
          if(results.length==1){
            connection.query('SELECT * FROM stm_mae_usuario_usae',(error,results)=>{
              if(error){
                throw error;
              }else{
                console.log("Entre a la alerta de duplicacion");
                if(req.session.loggedin == true){
                  name = req.session.name;
                  res.render('usuarios', { title: 'Usuarios',results:results,
                  alert: true,
                  alertTitle: "¡Error!",
                  alertMessage: "¡Ya existe un registro con la ID o el correo que se desea ingresar!",
                  alertIcon:'error',
                  showConfirmButton: true,
                  timer: 5000,
                  ruta: 'users',
                  name:name
              });
                }else{
                  res.render('index');
                }
              }
            })
          }else{
            connection.query('INSERT INTO stm_mae_usuario_usae SET ?',{ID_USUARIO_PK:ID_USUARIO_PK,TXT_NOMBRE:TXT_NOMBRE, 
              TXT_APP:TXT_APP, TXT_APM:TXT_APM, TXT_EMAIL:TXT_EMAIL ,TXT_PASSWORD:passwordHash, 
              ID_TIPO_USUARIO_FK:ID_TIPO_USUARIO_FK},async(error,results)=>{
                if(error){
                  res.json(error);
                }else{
                  connection.query('SELECT * FROM stm_mae_usuario_usae',(error,results)=>{
                    if(error){
                      res.json(error);
                    }else{
                      if(req.session.loggedin==true){
                        name=req.session.name;
                        res.render('usuarios',{
                          results:results,
                          title: 'Usuarios',
                          alert: true,
                          alertTitle: "Usuario Agregado",
                          alertMessage: "¡El usuario se agrego correctamente!",
                          alertIcon: 'success',
                          showConfirmButton: false,
                          timer: 3000,
                          ruta: 'users',
                          name: name
                        });
                      }else{
                        res.render('index');
                      }
                    }
                  });
                }
              });
          }
        }
      })
};


controller.deleteUser=((req,res) =>{
  const id = req.params.ID_USUARIO_PK;
  connection.query('DELETE FROM stm_mae_usuario_usae WHERE ID_USUARIO_PK = ?',[id],async(error, results)=>{
    if(error){
        res.json(error);
    }else{   
        res.redirect('/users');  
            }
        });           
});


controller.editUser=((req,res) =>{
  const id = req.params.ID_USUARIO_PK;
  connection.query('SELECT * FROM stm_mae_usuario_usae WHERE ID_USUARIO_PK = ?',[id],(error,results)=>{
    if(error){
      throw error;
    }else{
      if(req.session.loggedin == true){
        res.render('editUsuarios', { title: 'Editar Usuarios',user:results[0]});
      }else{
        res.render('index');
      }
    }
  })
});


controller.updateUser=((req,res) =>{
  const ID_USUARIO_PK = req.body.id;
    const TXT_NOMBRE = req.body.nombre;
    const TXT_APP = req.body.app;
    const TXT_APM = req.body.apm;
    const TXT_EMAIL = req.body.email;
    const ID_TIPO_USUARIO_FK = req.body.selectTipoUsuario;
  connection.query('UPDATE stm_mae_usuario_usae SET ? WHERE ID_USUARIO_PK = ? ',[{ID_USUARIO_PK:ID_USUARIO_PK, TXT_NOMBRE:TXT_NOMBRE, 
    TXT_APP:TXT_APP, TXT_APM:TXT_APM, TXT_EMAIL:TXT_EMAIL ,ID_TIPO_USUARIO_FK:ID_TIPO_USUARIO_FK},ID_USUARIO_PK],(err,results)=>{
      if(err){
          res.json(err);
      }else{
        res.redirect('/users');
      }
  });
});


module.exports =controller;