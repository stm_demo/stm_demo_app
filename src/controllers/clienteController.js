const express = require('express');
const controller = {};
const connection = require('../database/db');

controller.QueryDatatable=((req,res) => {
    connection.query('SELECT * FROM stm_mae_cliente',(error,results)=>{
        if(error){
          throw error;
        }else{
          if(req.session.loggedin == true){
            name = req.session.name;
            res.render('clientes', { title: 'Clientes',results:results,name:name});
          }else{
            res.render('index');
          }
        }
      })
});


controller.saveCliente=((req,res) =>{
    const curp = req.body.curp;
    const apepat = req.body.apepat;
    const apemat = req.body.apemat;
    const nombre = req.body.nombre;
    const calle = req.body.calle;
    const colonia = req.body.colonia;
    const email = req.body.email;
    const celular = req.body.celular;
    const selectSexo = req.body.selectSexo;
    const selectEstudio = req.body.selectEstudio;
    const selectTipoCliente = req.body.selectTipoCliente;
    connection.query('SELECT * FROM stm_mae_cliente WHERE ID_CURP_PK = ? OR TXT_EMAIL = ?',[curp,email],(error,results)=>{
    //connection.query('SELECT * FROM stm_mae_cliente WHERE ID_CURP_PK = ?',[curp],(error,results)=>{
      if(error){
        throw error;
      }else{
        if(results.length == 1){
          connection.query('SELECT * FROM stm_mae_cliente',(error,results)=>{
            if(error){
              throw error;
            }else{
              if(req.session.loggedin == true){
                name = req.session.name;
                res.render('clientes', { title: 'Clientes',results:results,
                alert: true,
                alertTitle: "¡Error!",
                alertMessage: "¡Ya existe un registro con la curp o el correo que se desea ingresar!",
                alertIcon:'error',
                showConfirmButton: true,
                timer: 5000,
                ruta: 'client',
                name:name
            });
              }else{
                res.render('index');
              }
            }
          })
        }
        else{
         connection.query('INSERT INTO stm_mae_cliente SET ?',{ID_CURP_PK:curp,TXT_NOMBRE:nombre,TXT_APP:apepat,TXT_APM:apemat,
          TXT_CALLE:calle,TXT_COL:colonia,TXT_EMAIL:email,TXT_CEL:celular,TXT_SEXO:selectSexo,TXT_GRADO_ESTUDIO:selectEstudio, 
          BIT_TIPO_CLIENTE:selectTipoCliente},async(err,results)=>{
              if(err){
                  res.json(err);
              }else{
                connection.query('SELECT * FROM stm_mae_cliente',(error,results)=>{
                  if(error){
                    throw error;
                  }else{
                    if(req.session.loggedin == true){
                      name = req.session.name;
                      res.render('clientes', { title: 'Clientes',results:results,
                      alert: true,
                      alertTitle: "¡Correcto!",
                      alertMessage: "¡El cliente se agregó correctamente!",
                      alertIcon:'success',
                      showConfirmButton: false,
                      timer: 5000,
                      ruta: 'client',
                      name:name
                      });
                    }else{
                      res.render('index');
                    }
                  }
                });
              }
          });
        }
      }
    })
});

controller.editCliente=((req,res) =>{
  const curp_id = req.params.ID_CURP_PK;
  connection.query('SELECT * FROM stm_mae_cliente WHERE ID_CURP_PK = ?',[curp_id],(error,results)=>{
    if(error){
      throw error;
    }else{
      if(req.session.loggedin == true){
        res.render('edicionClientes', { title: 'Editar Cliente',clientes:results[0],name:name});
      }else{
        res.render('index');
      }
    }
  })
});


controller.actualizarCliente=((req,res) =>{
  const curp = req.body.curp;
  const apepat = req.body.apepat;
  const apemat = req.body.apemat;
  const nombre = req.body.nombre;
  const calle = req.body.calle;
  const colonia = req.body.colonia;
  const email = req.body.email;
  const celular = req.body.celular;
  const selectSexo = req.body.selectSexo;
  const selectEstudio = req.body.selectEstudio;
  const selectTipoCliente = req.body.selectTipoCliente;
  connection.query('UPDATE stm_mae_cliente SET ? WHERE ID_CURP_PK = ? ',[{ID_CURP_PK:curp,TXT_NOMBRE:nombre,TXT_APP:apepat,TXT_APM:apemat,
  TXT_CALLE:calle,TXT_COL:colonia,TXT_EMAIL:email,TXT_CEL:celular,TXT_SEXO:selectSexo,TXT_GRADO_ESTUDIO:selectEstudio, 
  BIT_TIPO_CLIENTE:selectTipoCliente},curp],(err,results)=>{
      if(err){
          res.json(err);
      }else{
        if(req.session.loggedin == true){
          name = req.session.name;
          res.redirect('/client');
        }else{
          res.render('index');
        }
      }
  });
});





controller.eliminarCliente=((req,res) =>{
  const curp_id = req.params.ID_CURP_PK;
  connection.query('DELETE FROM stm_mae_turno WHERE ID_CURP_FK = ?',[curp_id]);
  connection.query('DELETE FROM stm_mae_cliente WHERE ID_CURP_PK = ?',[curp_id],async(err,results)=>{
      if(err){
          res.json(err);
      }else{
        if(req.session.loggedin == true){
          name = req.session.name;
          res.redirect('/client');
        }else{
          res.render('index');
        }
      }
  });
});



module.exports =controller;