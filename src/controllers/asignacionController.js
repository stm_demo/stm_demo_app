const controller = {};
const connection = require('../database/db');

controller.addTurno=(req,res) =>{
    name = req.session.name;
    let detalles = req.body.detalles;
    let estado = 'Por Atender';
    const hoy = new Date();
    var mes = hoy.getMonth() + 1; 
    var fecha = hoy.getFullYear() +'-'+ mes +'-'+ hoy.getDate();
    var hora = hoy.getHours() +':'+ hoy.getMinutes() +':'+ hoy.getSeconds();
    let lugar = 'USAE-DH';
    let selectPersonal = req.body.selectPersonal;
    let selectServicio = req.body.selectServicio;
    let selectCliente = req.body.selectCliente;
    let tiempoServicio = null;
    
    connection.query('INSERT INTO stm_mae_turno SET ?',{
        DSC_ASUNTO:detalles,
        TXT_ESTADO:estado,
        FCH_FECHA:fecha,
        HRA_TURNO:hora,
        TXT_LUGAR:lugar,
        TIME_TIEMPO_SERVICIO:tiempoServicio,
        ID_CURP_FK:selectCliente,
        ID_EMPLEADO_FK:selectPersonal,
        ID_SERVICIO_FK:selectServicio},async(err,results)=>{
        if(err){
            res.render('asignacion', { title: 'Asignación de Servicio',name:name });
        }else{
            if(req.session.loggedin == true){
                name = req.session.name;
                res.render('asignacion',{
                    title: 'Asignación de Servicio',
                    alert: true,
                    alertTitle: "Turno Registrado",
                    alertMessage: "Existe una encuesta de Satisfacción ¿Deseas contestarla?",
                    alertIcon:'success',
                    showConfirmButton: false,
                    name:name
                   });
            }else{
                res.render('index');
            }
           
        }
    });
};

controller.listSelctsExternos=((req,res) => {
    connection.query('SELECT * FROM stm_mae_servicio',async(err,servicios)=>{
        if(err){
            res.json(err);
        }else{

            connection.query('SELECT * FROM stm_mae_usuario_usae',async(err,personales)=>{
                if(err){
                    res.json(err);
                }else{
                    connection.query('SELECT * FROM stm_mae_cliente WHERE BIT_TIPO_CLIENTE=0',async(err,clientes)=>{
                        if(err){
                            res.json(err);
                        }else{
                            if(req.session.loggedin == true){
                                name = req.session.name;
                                res.render('asignacionExterno',{
                                    title: 'Asignación de Servicio | Usuario Externo',
                                    personal:personales,
                                    servicio:servicios,
                                    cliente:clientes,
                                    name:name
                                });
                            }else{
                                res.render('index');
                            }
                            
                        }
                    });
                }
            });
        }
    });
});

controller.listSelctsInternos=((req,res) => {
    connection.query('SELECT * FROM stm_mae_servicio',async(err,servicios)=>{
        if(err){
            res.json(err);
        }else{

            connection.query('SELECT * FROM stm_mae_usuario_usae',async(err,personales)=>{
                if(err){
                    res.json(err);
                }else{
                    connection.query('SELECT * FROM stm_mae_cliente WHERE BIT_TIPO_CLIENTE=1',async(err,clientes)=>{
                        if(err){
                            res.json(err);
                        }else{
                            if(req.session.loggedin == true){
                                name = req.session.name;
                                res.render('asignacionInterno',{
                                    title: 'Asignación de Servicio | Usuario Interno',
                                    personal:personales,
                                    servicio:servicios,
                                    cliente:clientes,
                                    name:name
                                });
                            }else{
                                res.render('index');
                            }
                        }
                    });
                }
            });
        }
    });
});

module.exports =controller;