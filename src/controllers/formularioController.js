const express = require('express');
const controller = {};
const connection = require('../database/db');




controller.consultaTurno=(req,res) =>{  
    if(req.session.loggedin == true){
        connection.query('SELECT * FROM stm_mae_turno ORDER BY ID_TURNO_PK DESC LIMIT 1', (err, turnos) =>{

            if(err){
                res.json(err);
            }else{
                    name = req.session.name;
                    res.render('formulario', {
                        title: 'Formulario de satisfacción',
                        data: turnos,
                        name:name
                     });
            }
        });

    }else{
        res.render('index');
    } 
};

controller.saveFormulario=(req,res) =>{

    const id_turno_fk = req.body.id_turno;


    let respuesta_total = (
        Math.round(
            (parseInt(req.body.respuesta_1) + 
            parseInt(req.body.respuesta_2) + 
            parseInt(req.body.respuesta_3)) / 3)
            );

    switch (respuesta_total) {
        case 1:
            respuesta = 'Satisfecho';
            break;
        case 2:
            respuesta = 'Regular';
            break;
        case 3:
            respuesta = 'Malo';
            break;
        default:
            break;
      }

    connection.query('INSERT INTO stm_cat_satisfaccion SET ?',{
        ID_TURNO_FK: id_turno_fk,
        TXT_RESPUESTA: respuesta
        },async(err)=>{
        if(err){
            res.json(err);
        }else{
            if(req.session.loggedin == true){
                name = req.session.name;
                res.render('asignacion',{
                    title: 'Formulario de satisfacción',
                    alert: true,
                    alertTitle: "Enviado",
                    alertMessage: "Se envio tu formulario de satisfacción",
                    alertIcon:'success',
                    showConfirmButton: '',
                    name:name
                   });

            }else{
                res.render('index');
            }
        }
    });
};


module.exports = controller;