const controller = {};
const connection = require('../database/db');

controller.listSelcts = ((req, res) => {
    connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
        if (err) {
            res.json(err);
        } else {
            if(req.session.loggedin == true){
                name = req.session.name;
                res.render('histServicio', {
                    title: 'Historial de Servicios',
                    error: 'no',
                    filtro: 0,
                    areasSlt: areas
                });
            }else{
                res.render('index');
            }
        }
    });
});

controller.getServicio = ((req, res) => {
    const {id} = req.params;
    connection.query('SELECT Turno.ID_TURNO_PK, Turno.DSC_ASUNTO, Turno.TXT_ESTADO, Turno.FCH_FECHA, Turno.HRA_TURNO, Turno.TXT_LUGAR, Serv.TXT_SERVICIO as Servicio , Area.TXT_NOMBRE as Area, Pers.TXT_NOMBRE as NombreP, Pers.TXT_APP as AppPP, Pers.TXT_APM as AppPM, Clint.TXT_NOMBRE as NombreC, Clint.TXT_APP as AppCP, Clint.TXT_APM as AppCM, Sat.TXT_RESPUESTA as Satisf FROM stm_mae_servicio AS Serv INNER JOIN `stm_mae_turno` AS Turno ON Serv.ID_SERVICIO_PK = Turno.ID_SERVICIO_FK INNER JOIN stm_mae_usuario_usae AS Pers ON Turno.ID_EMPLEADO_FK = Pers.ID_USUARIO_PK INNER JOIN stm_mae_cliente AS Clint ON Turno.ID_CURP_FK = Clint.ID_CURP_PK INNER JOIN stm_mae_area AS Area ON Serv.ID_AREA_FK = Area.ID_AREA_PK LEFT JOIN stm_cat_satisfaccion AS Sat ON Turno.ID_TURNO_PK = Sat.ID_TURNO_FK WHERE Turno.ID_TURNO_PK = ?',[id],(err, serviciosDetalles) => {
        if (err) {
            res.json(err);
        } else {
            connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
                if (err) {
                    res.json(err);
                } else {
                    if(req.session.loggedin == true){
                        name = req.session.name;
                        res.render('histServicio', {
                            title: 'Historial de Servicios',
                            filtro: 2,
                            areasSlt: areas,
                            serviciosDetalles: serviciosDetalles,
                            name:name
                        });
                    }else{
                        res.redirect('/');
                    }
                }
            });
        }
    });
});

controller.ApplyFilt = ((req, res) => {
    let area = req.body.selectArea;
    let tipo = req.body.selectTipo;
    let periodo = req.body.selectPeriodo;
    var areaSel = '';
    var errorArea = 0;
    var errorTipo = 0;
    var errorPer = 0;
    
    if(area == undefined){
        errorArea =1;
    }

    if(tipo == undefined){
        errorTipo =1;
    }

    if(periodo == undefined){
        errorPer =1;
    }

    if(errorArea == 1){
        //resErrorArea
        if(errorTipo == 1){
            //resErrorTipo
            if(errorPer == 1){
                //resErrorPer
                connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
                    if (err) {
                        res.json(err);
                    } else {
                        name = req.session.name;
                        res.render('histServicio', {
                            title: 'Historial de Servicios',
                            error: 'todos',
                            filtro: 0,
                            areasSlt: areas,
                            name:name
                        });
                    }
                });
            }
        }else if(errorPer == 1){
            connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
                if (err) {
                    res.json(err);
                } else {
                    name = req.session.name;
                    res.render('histServicio', {
                        title: 'Historial de Servicios',
                        error: 'areaPeriodo',
                        filtro: 0,
                        areasSlt: areas,
                        name:name
                    });
                }
            });
        }else{
            connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
                if (err) {
                    res.json(err);
                } else {
                    name = req.session.name;
                    res.render('histServicio', {
                        title: 'Historial de Servicios',
                        error: 'area',
                        filtro: 0,
                        areasSlt: areas,
                        name:name
                    });
                }
            });
        }
    }else if(errorTipo == 1){
        if(errorPer == 1){
            connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
                if (err) {
                    res.json(err);
                } else {
                    name = req.session.name;
                    res.render('histServicio', {
                        title: 'Historial de Servicios',
                        error: 'tipoPeriodo',
                        filtro: 0,
                        areasSlt: areas,
                        name:name
                    });
                }
            });
        }else{
            connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
                if (err) {
                    res.json(err);
                } else {
                    name = req.session.name;
                    res.render('histServicio', {
                        title: 'Historial de Servicios',
                        error: 'tipo',
                        filtro: 0,
                        areasSlt: areas,
                        name:name
                    });
                }
            });
        }
    }else if(errorPer == 1){
        connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
            if (err) {
                res.json(err);
            } else {
                name = req.session.name;
                res.render('histServicio', {
                    title: 'Historial de Servicios',
                    error: 'periodo',
                    filtro: 0,
                    areasSlt: areas,
                    name:name
                });
            }
        });
    }else{
        //Mensual
        if (tipo == 1) {
            connection.query('SELECT * FROM stm_mae_servicio AS Serv INNER JOIN `stm_mae_turno` AS Turno ON Serv.ID_SERVICIO_PK = Turno.ID_SERVICIO_FK INNER JOIN stm_mae_usuario_usae AS Pers ON Turno.ID_EMPLEADO_FK = Pers.ID_USUARIO_PK WHERE Serv.ID_AREA_FK = ' + area + ' AND MONTH(Turno.FCH_FECHA) = ' + periodo, async (err, servicios) => {
                if (err) {
                    res.json(err);
                } else {
                    connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
                        if (err) {
                            res.json(err);
                        } else {
                            for(var i=0; i<areas.length; i++){
                                if(areas[i].ID_AREA_PK == area){
                                    areaSel = new String(areas[i].TXT_NOMBRE);
                                }
                            };
                            name = req.session.name;
                            res.render('histServicio', {
                                title: 'Historial de Servicios',
                                filtro: 1,
                                areasSlt: areas,
                                areaSel: areaSel,
                                servicios: servicios,
                                name:name
                            });
                        }
                    });
                }
            });
            //Bimestral
        } else if (tipo == 2) {
            if (periodo == 1) {
                //Enero - Febrero
                connection.query('SELECT * FROM stm_mae_servicio AS Serv INNER JOIN `stm_mae_turno` AS Turno ON Serv.ID_SERVICIO_PK = Turno.ID_SERVICIO_FK INNER JOIN stm_mae_usuario_usae AS Pers ON Turno.ID_EMPLEADO_FK = Pers.ID_USUARIO_PK WHERE Serv.ID_AREA_FK = ' + area + ' AND MONTH(Turno.FCH_FECHA) = 1 OR MONTH(Turno.FCH_FECHA) = 2', async (err, servicios) => {
                    if (err) {
                        res.json(err);
                    } else {
                        connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
                            if (err) {
                                res.json(err);
                            } else {
                                for(var i=0; i<areas.length; i++){
                                    if(areas[i].ID_AREA_PK == area){
                                        areaSel = new String(areas[i].TXT_NOMBRE);
                                    }
                                };
                                name = req.session.name;
                                res.render('histServicio', {
                                    title: 'Historial de Servicios',
                                    filtro: 1,
                                    areasSlt: areas,
                                    areaSel:areaSel,
                                    servicios: servicios,
                                    name:name
                                });
                            }
                        });
                    }
                });
            } else if (periodo == 2) {
                //Marzo - Abril
                connection.query('SELECT * FROM stm_mae_servicio AS Serv INNER JOIN `stm_mae_turno` AS Turno ON Serv.ID_SERVICIO_PK = Turno.ID_SERVICIO_FK INNER JOIN stm_mae_usuario_usae AS Pers ON Turno.ID_EMPLEADO_FK = Pers.ID_USUARIO_PK WHERE Serv.ID_AREA_FK = ' + area + ' AND MONTH(Turno.FCH_FECHA) = 3 OR MONTH(Turno.FCH_FECHA) = 4', async (err, servicios) => {
                    if (err) {
                        res.json(err);
                    } else {
                        connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
                            if (err) {
                                res.json(err);
                            } else {
                                for(var i=0; i<areas.length; i++){
                                    if(areas[i].ID_AREA_PK == area){
                                        areaSel = new String(areas[i].TXT_NOMBRE);
                                    }
                                };
                                name = req.session.name;
                                res.render('histServicio', {
                                    title: 'Historial de Servicios',
                                    filtro: 1,
                                    areasSlt: areas,
                                    areaSel:areaSel,
                                    servicios: servicios,
                                    name:name
                                });
                            }
                        });
                    }
                });
            } else if (periodo == 3) {
                //Mayo - Junio
                connection.query('SELECT * FROM stm_mae_servicio AS Serv INNER JOIN `stm_mae_turno` AS Turno ON Serv.ID_SERVICIO_PK = Turno.ID_SERVICIO_FK INNER JOIN stm_mae_usuario_usae AS Pers ON Turno.ID_EMPLEADO_FK = Pers.ID_USUARIO_PK WHERE Serv.ID_AREA_FK = ' + area + ' AND MONTH(Turno.FCH_FECHA) = 5 OR MONTH(Turno.FCH_FECHA) = 6', async (err, servicios) => {
                    if (err) {
                        res.json(err);
                    } else {
                        connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
                            if (err) {
                                res.json(err);
                            } else {
                                for(var i=0; i<areas.length; i++){
                                    if(areas[i].ID_AREA_PK == area){
                                        areaSel = new String(areas[i].TXT_NOMBRE);
                                    }
                                };
                                name = req.session.name;
                                res.render('histServicio', {
                                    title: 'Historial de Servicios',
                                    filtro: 1,
                                    areasSlt: areas,
                                    areaSel:areaSel,
                                    servicios: servicios,
                                    name:name
                                });
                            }
                        });
                    }
                });
            } else if (periodo == 4) {
                //Julio - Agosto
                connection.query('SELECT * FROM stm_mae_servicio AS Serv INNER JOIN `stm_mae_turno` AS Turno ON Serv.ID_SERVICIO_PK = Turno.ID_SERVICIO_FK INNER JOIN stm_mae_usuario_usae AS Pers ON Turno.ID_EMPLEADO_FK = Pers.ID_USUARIO_PK WHERE Serv.ID_AREA_FK = ' + area + ' AND MONTH(Turno.FCH_FECHA) = 7 OR MONTH(Turno.FCH_FECHA) = 8', async (err, servicios) => {
                    if (err) {
                        res.json(err);
                    } else {
                        connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
                            if (err) {
                                res.json(err);
                            } else {
                                for(var i=0; i<areas.length; i++){
                                    if(areas[i].ID_AREA_PK == area){
                                        areaSel = new String(areas[i].TXT_NOMBRE);
                                    }
                                };
                                name = req.session.name;
                                res.render('histServicio', {
                                    title: 'Historial de Servicios',
                                    filtro: 1,
                                    areasSlt: areas,
                                    areaSel:areaSel,
                                    servicios: servicios,
                                    name:name
                                });
                            }
                        });
                    }
                });
            } else if (periodo == 5) {
                //Septiembre - Octubre
                connection.query('SELECT * FROM stm_mae_servicio AS Serv INNER JOIN `stm_mae_turno` AS Turno ON Serv.ID_SERVICIO_PK = Turno.ID_SERVICIO_FK INNER JOIN stm_mae_usuario_usae AS Pers ON Turno.ID_EMPLEADO_FK = Pers.ID_USUARIO_PK WHERE Serv.ID_AREA_FK = ' + area + ' AND MONTH(Turno.FCH_FECHA) = 9 OR MONTH(Turno.FCH_FECHA) = 10', async (err, servicios) => {
                    if (err) {
                        res.json(err);
                    } else {
                        connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
                            if (err) {
                                res.json(err);
                            } else {
                                for(var i=0; i<areas.length; i++){
                                    if(areas[i].ID_AREA_PK == area){
                                        areaSel = new String(areas[i].TXT_NOMBRE);
                                    }
                                };
                                name = req.session.name;
                                res.render('histServicio', {
                                    title: 'Historial de Servicios',
                                    filtro: 1,
                                    areasSlt: areas,
                                    areaSel:areaSel,
                                    servicios: servicios,
                                    name:name
                                });
                            }
                        });
                    }
                });
            } else if (periodo == 6) {
                //Noviembre - Diciembre
                connection.query('SELECT * FROM stm_mae_servicio AS Serv INNER JOIN `stm_mae_turno` AS Turno ON Serv.ID_SERVICIO_PK = Turno.ID_SERVICIO_FK INNER JOIN stm_mae_usuario_usae AS Pers ON Turno.ID_EMPLEADO_FK = Pers.ID_USUARIO_PK WHERE Serv.ID_AREA_FK = ' + area + ' AND MONTH(Turno.FCH_FECHA) = 11 OR MONTH(Turno.FCH_FECHA) = 12', async (err, servicios) => {
                    if (err) {
                        res.json(err);
                    } else {
                        connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
                            if (err) {
                                res.json(err);
                            } else {
                                for(var i=0; i<areas.length; i++){
                                    if(areas[i].ID_AREA_PK == area){
                                        areaSel = new String(areas[i].TXT_NOMBRE);
                                    }
                                };
                                name = req.session.name;
                                res.render('histServicio', {
                                    title: 'Historial de Servicios',
                                    filtro: 1,
                                    areasSlt: areas,
                                    areaSel:areaSel,
                                    servicios: servicios,
                                    name:name
                                });
                            }
                        });
                    }
                });
            }
        //Semestral
        }else if (tipo == 3){
            if(periodo == 1){
                //Enero - Junio
                connection.query('SELECT * FROM stm_mae_servicio AS Serv INNER JOIN `stm_mae_turno` AS Turno ON Serv.ID_SERVICIO_PK = Turno.ID_SERVICIO_FK INNER JOIN stm_mae_usuario_usae AS Pers ON Turno.ID_EMPLEADO_FK = Pers.ID_USUARIO_PK WHERE Serv.ID_AREA_FK = ' + area + ' AND MONTH(Turno.FCH_FECHA) = 1 OR MONTH(Turno.FCH_FECHA) = 2 OR MONTH(Turno.FCH_FECHA) = 3 OR MONTH(Turno.FCH_FECHA) = 4 OR MONTH(Turno.FCH_FECHA) = 5 OR MONTH(Turno.FCH_FECHA) = 6', async (err, servicios) => {
                    if (err) {
                        res.json(err);
                    } else {
                        connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
                            if (err) {
                                res.json(err);
                            } else {
                                for(var i=0; i<areas.length; i++){
                                    if(areas[i].ID_AREA_PK == area){
                                        areaSel = new String(areas[i].TXT_NOMBRE);
                                    }
                                };
                                name = req.session.name;
                                res.render('histServicio', {
                                    title: 'Historial de Servicios',
                                    filtro: 1,
                                    areasSlt: areas,
                                    areaSel:areaSel,
                                    servicios: servicios,
                                    name:name
                                });
                            }
                        });
                    }
                });
            }else if (periodo == 2){
                //Julio - Diciembre
                connection.query('SELECT * FROM stm_mae_servicio AS Serv INNER JOIN `stm_mae_turno` AS Turno ON Serv.ID_SERVICIO_PK = Turno.ID_SERVICIO_FK INNER JOIN stm_mae_usuario_usae AS Pers ON Turno.ID_EMPLEADO_FK = Pers.ID_USUARIO_PK WHERE Serv.ID_AREA_FK = ' + area + ' AND MONTH(Turno.FCH_FECHA) = 7 OR MONTH(Turno.FCH_FECHA) = 8 OR MONTH(Turno.FCH_FECHA) = 9 OR MONTH(Turno.FCH_FECHA) = 10 OR MONTH(Turno.FCH_FECHA) = 11 OR MONTH(Turno.FCH_FECHA) = 12', async (err, servicios) => {
                    if (err) {
                        res.json(err);
                    } else {
                        connection.query('SELECT * FROM `stm_mae_area`', async (err, areas) => {
                            if (err) {
                                res.json(err);
                            } else {
                                for(var i=0; i<areas.length; i++){
                                    if(areas[i].ID_AREA_PK == area){
                                        areaSel = new String(areas[i].TXT_NOMBRE);
                                    }
                                };
                                name = req.session.name;
                                res.render('histServicio', {
                                    title: 'Historial de Servicios',
                                    filtro: 1,
                                    areasSlt: areas,
                                    areaSel:areaSel,
                                    servicios: servicios,
                                    name:name
                                });
                            }
                        });
                    }
                });
            }
        }
    }

    
});

module.exports = controller;