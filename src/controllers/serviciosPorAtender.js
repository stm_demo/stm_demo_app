const express = require('express');
const controller = {};
const connection = require('../database/db');



controller.listServicios = ((req, res) => {
    const id = req.session.userid;
    connection.query('SELECT * FROM stm_mae_turno WHERE TXT_ESTADO = "Por Atender" AND ID_EMPLEADO_FK = ?', [id], (err, servicios) => {
        if (err) {
            res.json(err);
        } else {
            if (req.session.loggedin == true) {
                name = req.session.name;
                res.render('serviciosPorAtender', {
                    title: 'Servicios por Atender',
                    servicios:servicios,
                    name:name
                });
            } else {
                res.render('index');
            }
        }
    });
});




module.exports = controller;