const express = require('express');
const router = express.Router();

/* Importación de controlador */
const formularioController = require('../controllers/formularioController');

//Ruta de formulario atencion
router.get('/form', formularioController.consultaTurno);

router.post('/agregarFormulario', formularioController.saveFormulario);

module.exports = router;

