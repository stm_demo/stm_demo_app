var express = require('express');
const router = express.Router();

var app = express();

router.get('/', (req, res) => {
  res.render('index', { title: 'Signin' });
});

router.get('/inicio', (req, res) => {
  if(req.session.loggedin == true){
    name = req.session.name;
    res.render('paginaInicio', { title: 'Pagina de bienvenida de la aplicación web',name:name });
  }else{
    res.render('index');
  }
});


module.exports = router;
