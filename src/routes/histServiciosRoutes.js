const express = require('express');
const router = express.Router();

/* Importación de controlador */
const histServiciosController = require('../controllers/histServiciosController');

/* Rutas Asignacion */
router.get('/hist',histServiciosController.listSelcts);

router.post('/histFilt',histServiciosController.ApplyFilt);

router.get('/histShow/:id',histServiciosController.getServicio);


module.exports = router;