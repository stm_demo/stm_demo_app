const express = require('express');
const router = express.Router();

/* Importación de controlador */
const clienteController = require('../controllers/clienteController');

//Rutas Clientes
router.get('/edicionClientes', (req, res) => { 
    if(req.session.loggedin == true){
        name = req.session.name;
        res.render('edicionClientes', { title: 'Editar Cliente',name:name });
    }else{
        res.render('index');
    }
});

router.get('/client', clienteController.QueryDatatable);

router.post('/agregarCliente', clienteController.saveCliente);

router.get('/editarCliente/:ID_CURP_PK',clienteController.editCliente);

router.post('/updateCliente',clienteController.actualizarCliente);

router.get('/eliminarCliente/:ID_CURP_PK',clienteController.eliminarCliente);

module.exports = router;

