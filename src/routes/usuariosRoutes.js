const express = require('express');
const router = express.Router();

/* Importación de controlador */
const usuariosController = require('../controllers/usuariosController');

/*Rutas Usuarios */
router.get('/users',usuariosController.users);

router.post('/addUser',usuariosController.addUser);

router.get('/deleteUser/:ID_USUARIO_PK',usuariosController.deleteUser);

router.get('/editUser/:ID_USUARIO_PK',usuariosController.editUser);

router.post('/updateUser',usuariosController.updateUser);

router.get('/editUsuarios', (req, res) => {
    if(req.session.loggedin == true){
        name = req.session.name;
        res.render('editUsuarios', { title: 'Editar Usuario'});
    }else{
        res.render('index');
    }
});


module.exports = router;
