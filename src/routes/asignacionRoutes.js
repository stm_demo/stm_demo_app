const express = require('express');
const router = express.Router();

/* Importación de controlador */
const asignacionController = require('../controllers/asignacionController');

/* Rutas Asignacion */
router.get('/asig',  (req, res) => {
    if(req.session.loggedin == true){
        name = req.session.name;
        res.render('asignacion', { title: 'Asignación de Servicio',name:name });
    }else{
        res.render('index');
    }
});

router.get('/asigExt',asignacionController.listSelctsExternos);

router.get('/asigInt',asignacionController.listSelctsInternos);

router.post('/addTurno',asignacionController.addTurno);


module.exports = router;

