const express = require('express');
const router = express.Router();
const controller = require('../controllers/serviciosPorAtender');

router.get('/servicios',controller.listServicios);

module.exports = router;