
--
-- Volcado de datos para la tabla `stm_aux_cnto_tbajo`
--

INSERT INTO `stm_aux_cnto_tbajo` (`ID_CCT_PK`, `TXT_NOMBRE`, `NUM_ZONA`, `TXT_SECTOR`, `TXT_NIVEL`) VALUES
('CCT1', 'Centro de trabajo 1', 511, '12', 'Primaria');

--
-- Volcado de datos para la tabla `stm_mae_area`
--

INSERT INTO `stm_mae_area` (`ID_AREA_PK`, `TXT_NOMBRE`) VALUES
(1, 'SUBJEFATURA DE SERVICIOS AL PERSONAL'),
(2, 'SUBJEFATURA DE EFICIENCIA ADMINISTRATIVA'),
(3, 'SUBJEFATURA DE PLANEACION Y CONTROL ESCOLAR'),
(4, 'SUBJEFATURA DE MEJORA DE PROCESOS E INFORMÁTICA'),
(5, 'SUBJEFATURA DE PROMOCION Y SERVICIO PROFECIONAL DOCENTE');

--
-- Volcado de datos para la tabla `stm_mae_cliente`
--

INSERT INTO `stm_mae_cliente` (`ID_CURP_PK`, `TXT_NOMBRE`, `TXT_APP`, `TXT_APM`, `TXT_CALLE`, `TXT_COL`, `TXT_EMAIL`, `TXT_CEL`, `TXT_SEXO`, `TXT_GRADO_ESTUDIO`, `BIT_TIPO_CLIENTE`, `ID_CCT_FK`) VALUES
('SUOC980115HGTRLR00', 'Carlos Antonio', 'Suárez', 'Olvera', 'TULIPAM', 'Jardines de San Ignacio', 'carlosolvera98@hotmail.com', '4681151643', 'Masculino', 'Ingenieria', 1, NULL),
('SUOC980115HGTRLR05', 'Miriam ', 'Ramirez', 'Gloria', 'JF Carlos', 'San Luis Gonzaga', 'miriam@gmail.com', '1233456782', 'Femenino', 'Ingenieria', 0, NULL);

--
-- Volcado de datos para la tabla `stm_mae_servicio`
--

INSERT INTO `stm_mae_servicio` (`ID_SERVICIO_PK`, `TXT_SERVICIO`, `ID_AREA_FK`) VALUES
(1, 'Constacias de servicio', 1),
(2, 'Cotejo de documentos', 1),
(3, 'Credencialización', 1),
(4, 'Forte', 1),
(5, 'Ahorro solidario', 1),
(6, 'Designación de beneficiarios', 1),
(7, 'Seguro institucional', 1),
(8, 'Inscripción al Concurso de Ingreso o Promoción para Personal Docente ', 1),
(9, 'Distribución y Pago de Nómina', 1),
(10, 'Aclaraciones de pago', 1),
(11, 'Propuestas de contratación', 1),
(12, 'Validación de plantillas ', 1),
(13, 'Gastos de operación', 2),
(14, 'Entrega de libros de texto gratuitos ', 2),
(15, 'Robo o siniestro de bienes muebles', 2),
(16, 'Baja o Trasferencia en SINA ', 2),
(17, 'Modificación de servicio de energía eléctrica ', 2),
(18, 'Reposició de certificados de Primaria,Secundaria y Telesecundaria', 3),
(19, 'Historial Académico', 3),
(20, 'Alta y Baja de alumnos del nivel Preescolar, Primaria y Secundaria', 3),
(21, 'Asesoría y apoyo a la captura Estadística 911', 3),
(22, 'Registro de las necesidades de infrastructura educativa e incorporarlas al Programa Anual de Obra (PAO)', 3),
(23, 'Programa de Mantenimiento de Escuelas', 3),
(24, 'Microplaneación y PRODET: Gestión de las necesidades de personal administrativo y docente (promoción natural y expanción)', 3),
(25, 'Informe de calificaciones parciales de Sucundaria y Telesecundaria', 3),
(26, 'Correo institucional ', 4),
(27, 'Soporte técnico ', 4),
(28, 'Dictámen de equipos informáticos ', 4),
(29, 'Mantenimiento preventivo y correctivo a equipos informáticos', 4),
(30, 'Carrera magisterial', 5),
(31, 'Carrera administrativa', 5),
(32, 'IPAC (Programa Permanente para Elevar la Calidad en la Educación)', 5),
(33, 'Aclaración IPAC', 5);

--
-- Volcado de datos para la tabla `stm_tipo_usuario`
--

INSERT INTO `stm_tipo_usuario` (`ID_TIPO_USUARIO_PK`, `TXT_TIPO_USUARIO`) VALUES
(1, 'admin'),
(2, 'personal');

--
-- Volcado de datos para la tabla `stm_mae_usuario_usae`
--

INSERT INTO `stm_mae_usuario_usae` (`ID_USUARIO_PK`, `TXT_NOMBRE`, `TXT_APP`, `TXT_APM`, `TXT_EMAIL`, `TXT_PASSWORD`, `ID_TIPO_USUARIO_FK`) VALUES
('ASTM-01', 'Carlos', 'Suarez', 'Olvera', 'carlosolvera98@hotmail.com', '$2a$08$fiX.fk2GRznJCkRJV5b6UOhHBF2DIZ1nuu2HKFNq6kmw/n51nB1p2', 1),
('ASTM-02', 'Alan Eduardo', 'Herrera', 'Alvarado', 'alanherrera.2828@gmail.com', '$2a$08$2uG9elsN9PWSh8sUvAjKtOm6nAV0v.Ij5nNSLVZM1fdWuwdJQ94Ym', 1),
('ASTM-03', 'Juan Daniel', 'Aguilar ', 'Cano', 'dhaniele212@gmail.com', '$2a$08$paYncG1exHMjVx1eUfOejupMA16rnYpuABCQTnwemzA/HeLi3KT96', 1),
('ASTM-04', 'Jose Carmen', 'Moya', 'Tovar', 'jose.carmen.moya.tovar@gmail.com', '$2a$08$T3ptmuXj9KBh5VqDplAhyet6PHxQQoANi.HMiyEXnIAf6TZaM9rFm', 1),
('ASTM-05', 'Miriam', 'Ramirez', 'Gloria', 'miriamg.rg80@gmail.com', '$2a$04$Q6.BDJT82RP9IawI2l7WYej9.V7uyeUtmOkCwGQbVisLSudiNbJ36', 1),
('PSTM-01', 'Brandon Aldahir', 'Leon', 'Alvarado', 'brandonleon05@gmail.com', '$2a$08$WSa6llGx.ZBrlns73vHMEeDg5HQGwKQlBrWYsk.YPpf/0MgWojrl6', 2),
('PSTM-02', 'Leonardo Daniel', 'Ramirez', 'Reyes', 'leoreyes56@gmail.com', '$2a$08$v7XtJkTE4BwjF21AdbDByOp.zm79Kzxg.xaI/d7F62YuZOmpIzI8W', 2),
('PSTM-03', 'Suemy Alyn', 'Villafranco', 'Jimenez', 'suemyaly10@outlook.com', '$2a$08$ljF5oU0xhiJfVt6JqHXjQuAe5uDM9IHBU2ZX4YUz1v1kYGPNM46ri', 2),
('PSTM-04', 'Zoe Yutzil', 'Perez', 'Gloria', 'zoeyut@hotmail.com', '$2a$08$aOzSSTCbgLfIwP/TV00SJeJMQYFW/QMJibw27iVzGiqdIpC/KRgzS', 2);

--
-- Volcado de datos para la tabla `stm_mae_turno`
--

INSERT INTO `stm_mae_turno` (`ID_TURNO_PK`, `DSC_ASUNTO`, `TXT_ESTADO`, `FCH_FECHA`, `HRA_TURNO`, `TXT_LUGAR`, `TIME_TIEMPO_SERVICIO`, `ID_CURP_FK`, `ID_EMPLEADO_FK`, `ID_SERVICIO_FK`) VALUES
(28, 'Quiere dar clases', 'Por Atender', '2021-06-26', '15:29:30', 'USAE-DH', NULL, 'SUOC980115HGTRLR05', 'ASTM-01', 11),
(29, 'Robo de monitores en la telesecundaria 405', 'Por Atender', '2021-07-26', '15:46:41', 'USAE-DH', NULL, 'SUOC980115HGTRLR00', 'ASTM-02', 15),
(32, 'Por aumento de matricula', 'Por Atender', '2021-07-26', '16:56:57', 'USAE-DH', NULL, NULL, 'ASTM-03', 14);
