-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Servidor: sql5.freesqldatabase.com
-- Tiempo de generación: 27-07-2021 a las 05:57:13
-- Versión del servidor: 5.5.62-0ubuntu0.14.04.1
-- Versión de PHP: 7.0.33-0ubuntu0.16.04.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sql5427274`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stm_aux_cnto_tbajo`
--

CREATE TABLE `stm_aux_cnto_tbajo` (
  `ID_CCT_PK` varchar(50) NOT NULL,
  `TXT_NOMBRE` varchar(60) NOT NULL,
  `NUM_ZONA` smallint(6) NOT NULL,
  `TXT_SECTOR` varchar(60) NOT NULL,
  `TXT_NIVEL` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stm_cat_satisfaccion`
--

CREATE TABLE `stm_cat_satisfaccion` (
  `FOLIO_PK` int(11) NOT NULL,
  `ID_TURNO_FK` int(11) NOT NULL,
  `TXT_RESPUESTA` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stm_mae_area`
--

CREATE TABLE `stm_mae_area` (
  `ID_AREA_PK` int(6) NOT NULL,
  `TXT_NOMBRE` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stm_mae_cliente`
--

CREATE TABLE `stm_mae_cliente` (
  `ID_CURP_PK` varchar(50) NOT NULL,
  `TXT_NOMBRE` varchar(50) NOT NULL,
  `TXT_APP` varchar(50) NOT NULL,
  `TXT_APM` varchar(50) NOT NULL,
  `TXT_CALLE` varchar(60) NOT NULL,
  `TXT_COL` varchar(60) NOT NULL,
  `TXT_EMAIL` varchar(200) NOT NULL,
  `TXT_CEL` varchar(20) NOT NULL,
  `TXT_SEXO` varchar(11) NOT NULL,
  `TXT_GRADO_ESTUDIO` varchar(255) NOT NULL,
  `BIT_TIPO_CLIENTE` int(11) NOT NULL,
  `ID_CCT_FK` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stm_mae_servicio`
--

CREATE TABLE `stm_mae_servicio` (
  `ID_SERVICIO_PK` smallint(6) NOT NULL,
  `TXT_SERVICIO` varchar(200) NOT NULL,
  `ID_AREA_FK` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stm_mae_turno`
--

CREATE TABLE `stm_mae_turno` (
  `ID_TURNO_PK` int(11) NOT NULL,
  `DSC_ASUNTO` varchar(200) NOT NULL,
  `TXT_ESTADO` varchar(200) NOT NULL,
  `FCH_FECHA` date NOT NULL,
  `HRA_TURNO` time NOT NULL,
  `TXT_LUGAR` varchar(40) NOT NULL,
  `TIME_TIEMPO_SERVICIO` varchar(50) DEFAULT NULL,
  `ID_CURP_FK` varchar(50) DEFAULT NULL,
  `ID_EMPLEADO_FK` varchar(30) NOT NULL,
  `ID_SERVICIO_FK` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stm_mae_usuario_usae`
--

CREATE TABLE `stm_mae_usuario_usae` (
  `ID_USUARIO_PK` varchar(30) NOT NULL,
  `TXT_NOMBRE` varchar(50) NOT NULL,
  `TXT_APP` varchar(50) NOT NULL,
  `TXT_APM` varchar(50) NOT NULL,
  `TXT_EMAIL` varchar(200) NOT NULL,
  `TXT_PASSWORD` varchar(255) NOT NULL,
  `ID_TIPO_USUARIO_FK` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stm_tipo_usuario`
--

CREATE TABLE `stm_tipo_usuario` (
  `ID_TIPO_USUARIO_PK` int(11) NOT NULL,
  `TXT_TIPO_USUARIO` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `stm_aux_cnto_tbajo`
--
ALTER TABLE `stm_aux_cnto_tbajo`
  ADD PRIMARY KEY (`ID_CCT_PK`);

--
-- Indices de la tabla `stm_cat_satisfaccion`
--
ALTER TABLE `stm_cat_satisfaccion`
  ADD PRIMARY KEY (`FOLIO_PK`,`ID_TURNO_FK`),
  ADD KEY `_ID_TURNO_FK` (`ID_TURNO_FK`);

--
-- Indices de la tabla `stm_mae_area`
--
ALTER TABLE `stm_mae_area`
  ADD PRIMARY KEY (`ID_AREA_PK`);

--
-- Indices de la tabla `stm_mae_cliente`
--
ALTER TABLE `stm_mae_cliente`
  ADD PRIMARY KEY (`ID_CURP_PK`),
  ADD KEY `_ID_CCT_FK` (`ID_CCT_FK`);

--
-- Indices de la tabla `stm_mae_servicio`
--
ALTER TABLE `stm_mae_servicio`
  ADD PRIMARY KEY (`ID_SERVICIO_PK`),
  ADD KEY `STM_AREA_SERVICIO_FK` (`ID_AREA_FK`);

--
-- Indices de la tabla `stm_mae_turno`
--
ALTER TABLE `stm_mae_turno`
  ADD PRIMARY KEY (`ID_TURNO_PK`),
  ADD KEY `USER_STM_MAE_TURNO_FK` (`ID_CURP_FK`),
  ADD KEY `E_USAE_STM_MAE_TURNO_FK` (`ID_EMPLEADO_FK`),
  ADD KEY `SERVICIO_STM_MAE_TURNO_FK` (`ID_SERVICIO_FK`);

--
-- Indices de la tabla `stm_mae_usuario_usae`
--
ALTER TABLE `stm_mae_usuario_usae`
  ADD PRIMARY KEY (`ID_USUARIO_PK`),
  ADD KEY `_ID_TIPO_USUARIO_FK` (`ID_TIPO_USUARIO_FK`);

--
-- Indices de la tabla `stm_tipo_usuario`
--
ALTER TABLE `stm_tipo_usuario`
  ADD PRIMARY KEY (`ID_TIPO_USUARIO_PK`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `stm_cat_satisfaccion`
--
ALTER TABLE `stm_cat_satisfaccion`
  MODIFY `FOLIO_PK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de la tabla `stm_mae_area`
--
ALTER TABLE `stm_mae_area`
  MODIFY `ID_AREA_PK` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `stm_mae_servicio`
--
ALTER TABLE `stm_mae_servicio`
  MODIFY `ID_SERVICIO_PK` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `stm_mae_turno`
--
ALTER TABLE `stm_mae_turno`
  MODIFY `ID_TURNO_PK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `stm_cat_satisfaccion`
--
ALTER TABLE `stm_cat_satisfaccion`
  ADD CONSTRAINT `_ID_TURNO_FK` FOREIGN KEY (`ID_TURNO_FK`) REFERENCES `stm_mae_turno` (`ID_TURNO_PK`);

--
-- Filtros para la tabla `stm_mae_cliente`
--
ALTER TABLE `stm_mae_cliente`
  ADD CONSTRAINT `_ID_CCT_FK` FOREIGN KEY (`ID_CCT_FK`) REFERENCES `stm_aux_cnto_tbajo` (`ID_CCT_PK`);

--
-- Filtros para la tabla `stm_mae_servicio`
--
ALTER TABLE `stm_mae_servicio`
  ADD CONSTRAINT `STM_AREA_SERVICIO_FK` FOREIGN KEY (`ID_AREA_FK`) REFERENCES `stm_mae_area` (`ID_AREA_PK`);

--
-- Filtros para la tabla `stm_mae_turno`
--
ALTER TABLE `stm_mae_turno`
  ADD CONSTRAINT `E_USAE_STM_MAE_TURNO_FK` FOREIGN KEY (`ID_EMPLEADO_FK`) REFERENCES `stm_mae_usuario_usae` (`ID_USUARIO_PK`),
  ADD CONSTRAINT `SERVICIO_STM_MAE_TURNO_FK` FOREIGN KEY (`ID_SERVICIO_FK`) REFERENCES `stm_mae_servicio` (`ID_SERVICIO_PK`),
  ADD CONSTRAINT `USER_STM_MAE_TURNO_FK` FOREIGN KEY (`ID_CURP_FK`) REFERENCES `stm_mae_cliente` (`ID_CURP_PK`);

--
-- Filtros para la tabla `stm_mae_usuario_usae`
--
ALTER TABLE `stm_mae_usuario_usae`
  ADD CONSTRAINT `_ID_TIPO_USUARIO_FK` FOREIGN KEY (`ID_TIPO_USUARIO_FK`) REFERENCES `stm_tipo_usuario` (`ID_TIPO_USUARIO_PK`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
